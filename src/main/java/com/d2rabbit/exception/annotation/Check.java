package com.d2rabbit.exception.annotation;

import java.lang.annotation.*;

/**
 * 声明参数的校验提示信息（目前只有空校验）
 * */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.PARAMETER,ElementType.LOCAL_VARIABLE})
public @interface Check {
	/**
	 * 属性字段注解抛出的消息
	 * */
	String msg() default "参数异常";

}
