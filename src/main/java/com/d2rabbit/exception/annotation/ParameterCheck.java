package com.d2rabbit.exception.annotation;

import com.d2rabbit.exception.base.BaseBean;

import java.lang.annotation.*;

/**
 * 参数校验注解-目前仅支持参数的空字符检验
 * */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD})
public @interface ParameterCheck {
    /**
     * 参数类型
     * */
	Class<? extends BaseBean> bean()  default BaseBean.class;

}
