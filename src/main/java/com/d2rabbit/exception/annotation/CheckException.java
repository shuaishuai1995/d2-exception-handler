package com.d2rabbit.exception.annotation;


import com.d2rabbit.exception.exception.DefaultException;

import java.lang.annotation.*;

/**
 * 非业务异常处理注解
 * **/
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface CheckException {

    int code() default 0;

    String message() default "未知异常";

    Class<? extends Exception> exception() default DefaultException.class;

}
