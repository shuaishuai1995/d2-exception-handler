package com.d2rabbit.exception.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.d2rabbit.**"})
public class AppConfig {
}
