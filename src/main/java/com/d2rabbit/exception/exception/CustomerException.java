package com.d2rabbit.exception.exception;


import com.d2rabbit.exception.base.LocalError;

import java.io.Serializable;

/**
 * 自定义异常抽象类
 *
 * */
public  class CustomerException  extends RuntimeException implements Serializable{

    private static final long serialVersionUID = 1L;
    private Integer exceptionCode;
    private  String message;

    public CustomerException(Integer code, String msg) {
        this.exceptionCode = code;
        this.message = msg;
    }


    public CustomerException(LocalError localError) {
        this.exceptionCode =localError.getCode();
        this.message = localError.getMsg();
    }


    public Integer getExceptionCode() {
        return exceptionCode;
    }

    public void setExceptionCode(Integer exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 抛出自定义异常
     * */
    public static void throwCustomerException(LocalError localError){
        throw  new CustomerException(localError);
    }

    public static void throwCustomerException(String msg,Integer code){
        throw  new CustomerException(code,msg);
    }

}
