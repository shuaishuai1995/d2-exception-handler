package com.d2rabbit.exception.exception.handler;

import lombok.Data;

@Data
public class ExceptionResult {
	private Integer code;
	private String message;
	private boolean isSuccess;

	public ExceptionResult(Integer code, String message) {
		this.code = code;
		this.message = message;
		this.isSuccess = false;
	}

	public ExceptionResult(Integer code  ,String msg,boolean isSuccess) {
		this.code = code;
		this.message = msg;
		this.isSuccess = isSuccess;
	}

}
