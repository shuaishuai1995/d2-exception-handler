package com.d2rabbit.exception.exception.handler;

import com.d2rabbit.exception.base.LocalError;
import com.d2rabbit.exception.exception.CustomerException;
import com.d2rabbit.exception.exception.DefaultException;

import java.util.List;

/**
 * 异常验证器
 * 自定义异常类型 ，继承CustomerException，抛出异常
 * */
public interface Validator {


     void throwCustomerException(LocalError localError, Class<? extends CustomerException> clazz) ;

     void throwCustomerException(String msg,Integer code) ;
     /**
      * 默认异常跑出
      * */
     static void throwDefaultException( String msg) throws DefaultException {
         throw new DefaultException(msg);
     }

     void isTrue(boolean expression, LocalError localError, Class<? extends CustomerException> clazz) ;

     void isNull(Object obj, LocalError localError, Class<? extends CustomerException> clazz) ;

     void notNull(Object obj, LocalError localError, Class<? extends CustomerException> clazz);

    void notEmpty(String str, LocalError localError, Class<? extends CustomerException> clazz);

    void notEmpty(List list, LocalError localError, Class<? extends CustomerException> clazz);
}
