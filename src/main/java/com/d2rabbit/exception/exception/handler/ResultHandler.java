package com.d2rabbit.exception.exception.handler;

public class ResultHandler {


	private static final Integer EXCEPTION_CODE_DEFAULT = 500;

	public static ExceptionResult returnResult(String message){
		return new ExceptionResult(EXCEPTION_CODE_DEFAULT,message);
	}

	public static ExceptionResult returnResult(Integer code , String message){
		return new ExceptionResult(code,message);
	}

	public static ExceptionResult returnResult(Integer code , String message, Boolean isSuccess){
		return new ExceptionResult(code,message,isSuccess);
	}

}
