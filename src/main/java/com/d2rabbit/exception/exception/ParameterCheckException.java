package com.d2rabbit.exception.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ParameterCheckException extends RuntimeException{
	private static final Integer PARAMETER_CHECK_CODE = -1;
	private Integer exceptionCode;
	private String message;
	public ParameterCheckException(String msg) {
		super();
		this.exceptionCode = PARAMETER_CHECK_CODE ;
		this.message = msg;
	}

}
