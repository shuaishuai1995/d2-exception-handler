package com.d2rabbit.exception.exception;

/**
 * 非自定义异常抛出
 * */
public class DefaultException extends Exception {
    private static final long serialVersionUID = -8371858782555695073L;


	private Integer exceptionCode;


	public Integer getExceptionCode() {
		return exceptionCode;
	}

	public void setExceptionCode(Integer exceptionCode) {
		this.exceptionCode = exceptionCode;
	}


    public DefaultException() {
        super();
    }

    public DefaultException(String message) {
        super(message);
		this.exceptionCode = 0;
    }

    public DefaultException(String message, Throwable cause) {
        super(message, cause);
    }

    public DefaultException(Throwable cause) {
        super(cause);
    }

	public DefaultException(String message,Integer exceptionCode) {
		super(message);
		this.exceptionCode = exceptionCode;
	}


    protected DefaultException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
