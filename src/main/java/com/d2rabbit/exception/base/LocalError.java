package com.d2rabbit.exception.base;
/**
 * 统一错误异常接口 该接口继承自通用枚举继承接口
 * */
public interface LocalError {
	/**
     * 获取错误码
     * */
	Integer getCode();
	/**
     * 获取错误信息
     * */
	String getMsg();
}
